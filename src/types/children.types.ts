import { ReactNode } from "react";

interface Children {
    children: ReactNode
};

export default Children;