import { ReactNode } from "react";

//theme
import "primereact/resources/themes/lara-light-indigo/theme.css";     
//core
import "primereact/resources/primereact.min.css";                                       
// css
import "/node_modules/primeflex/primeflex.css"        
import SideBarMenu from "../SideBarMenu";
import Children from "@/types/children.types";

export default function DefaultLayout({children} : Children) {
    return (
        // Grid Layout
        <div className="h-full grid">  
            <div className="bg-gray-800 m-2 p-2">
                {<SideBarMenu/>}                            
            </div>          
            <div className="col"></div>
        </div>
    )
}