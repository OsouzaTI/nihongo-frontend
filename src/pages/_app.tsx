import { AppProps } from 'next/app';
import DefaultLayout from '../components/layouts/Layout';

import "src/styles/global.css"

export default function Nihongo({ Component, pageProps } : AppProps) {
  return (
    <DefaultLayout >
      <Component {...pageProps} />
    </DefaultLayout>
  );
}