import { CustomCalendar } from "@/components/CustomCalendar";
import { useState } from "react";

export default function Index() {

    const [date, setDate] = useState<Date>()

    return (
        <CustomCalendar value={date} onChange={(e) => setDate(e.value)}/>
    )
}